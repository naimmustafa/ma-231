# MA-231 Git

This documentation covers all of the requested functions of the git commands (MA-231) and more.
Git is a open source version system control software that allows users work much more easy and work in groups on their projects. Users can work with git locally and remotely as well. A visual chart attached for work areas of git below.  

![git workflow chart](./Git-Workflow-Wd.png)

This workflow chart is a great source to visualise what is happening following subtitles

## Creating and Cloning Repository

First ting to do before creating a local repository is [download](https://git-scm.com/) git to the local machine. Once that step completed user can navigate to the folder where they want to create the repository and type following command to the terminal to initialise the local repository.
```
git init <repository name>
```

In order to create a new remote repository, developers need to sign up a git service such as bitbucket, github etc. After creating the repository users need to clone the repository to their own computer. Git command for cloning:
```
git clone https://{url}
```

This command will create a copy of the repository folder on users computer.

Once users create their repository either local or remote repos, by default first HEAD of the repository called master.

## Difference Between Local and Remote Repositories

As a  structure there is not any difference between them. However remote repositories allows user to store their changes online and make accessible for anybody. Also they require one extra step (-git push) which is explained under Pushing Files title. On the other hand local repositories limited with users workstation at it is called local.

## Staging Files

Staging files are the files that users have done working on them and they are ready to send to their git repository. Git command for staging files:
- for all changed files;
```
git add .
```

- for specific files;
```
git add <file>
```

## Committing Files

Before users sending their changes to their repository and update it. Git requires a record keeping by committing the file. When users commits their work, git creates a unique id for the commit where users can keep track of the changes they made. It is always best to add a message to a commit in matter of keep track. Git command for committing with a message:

- for all staged files;
```
git commit -m "your message"
```

-for specific files;
```
git commit <file> -m "your message"
```

## Difference Between Staged and Unstaged Files

Staged files are the files ready to commit. When working on a branch, if developers needs to leave that branch but they have partially done the work and have both finished and unfinished files then they can stage finished files with ```git add <finished files>``` . After this if user runs git commit command only the staged files will be committed.

## Discarding Changes

If a user for some reason wants to discard changes on staged and committed files, they can reset all files or specific file.
Git command for reseting:

- reset on staged files
```
git reset  or  git reset <file>
```

- reset on committed files
```
git reset --soft HEAD~1  
```
**note**: see Reflog for HEAD

## Pushing Files

After users commit in other words keep track of their work, they need to apply those changes by pushing their work and update their remote repository. Git command for push:
```
git push
```

this command will push the work to current brach of the remote repository

## Creating Branch

Purpose of creating a new branch is to apply changes without breaking the master branch until user satisfies with the changes have been made. Also this allows multiple people to work on the same repository without affecting each others work. Git command to create a new branch:

- this command will create a local branch;
```
git checkout -b <new_branch_name>
```

- to make local branch to remote branch we need to push it;
```
git push <remote_name> <branch_name>  eg. git push origin :new-branch
```

## Switching Branch

When users working on a branch and for some reason they need to leave that project and carry on work on another branch, they can switch branch. Git command for switching branch:
```
git checkout <branch_name>
```

to be able to see which branch the user currently on *see Check Status for more info*;
```
git status
```

## Stahsing

Before users switching the branch if they have unfinished work, in order to save those changes without pushing them to the repository, they can save those changes locally with stashing them. Git command for stashing:
```
git stash
```

After they come back on stashed branch, to see their different stashed if there are any;

```
git stash list
```
To apply stashed changes;

```
git stash apply
```

## Pulling a Branch

If a user checkout to a branch or if somebody else working on the same branch and that user wants to have latest changes for both scenarios they need to pull it from the repository. Git command for pulling:

```
git pull
```


## Deleting Branch

When users decide to get rid of a branch for some reason they can delete that local or remote branch. Command for deleting a branch:

- deleting a local branch
```
git branch -d <local_branch>
```

- deleting a remote branch
```
git push origin :<remote_branch>
```

## Merging Branches

Once a user finished the work on a branch and wants to apply all the changes to the master branch, they need to merge modified branch to master branch. Method I use for merging:

- first merge master into my_branch and check if anything breaks in the code. If everything is okay I switch to master branch and merge my_branch into master branch.
```
- git checkout my_branch
- git pull
- git merge master
- commit -m "reason for this merge"
- git push
- git checkout master
- git pull
- git merge my_branch
- git commit -m "reason for this merge"
- git push
```  

## Solving Merge Conflicts

If there are a conflict between files during the merge, users can select the version of the code they want to use trough their code editor or from repository.

[example](https://user-images.githubusercontent.com/401128/27737141-6f7ae2f6-5d7d-11e7-9312-87c8611e7328.png)

## Checking Status

When user lost track of what is going on their branch or they wants to know which branch they are on, they can check status by following command:
```
git status
```
This command will display;

- Current brach and if it is up to date with the remote branch
- Status of the working the working tree
  - If there are unstaged files
  - If user have staged files but have not commit them yet
  - If user have committed changes but have not pushed them yet
  - If the file is new or modified

## Ignoring Files (.gitignore)

If for some reason a user do not want to push a file to their remote repository but they want to keep that file inside the local repository, they can use .gitignore files which ignores those files trough the workflow of git as it is self explanatory from its name. But this change on the .gitignore file must be committed and pushed to the repository. Since files starting with "." is not usually visible by default, it would be best to keep files to ignore separately unless it is necessary.

## Reflog and Log

Monitoring all user activity with git is possible by following command:
```
git log
```
This command will [display](http://itsananderson.blob.core.windows.net/post-images/git-log-medium.png) all activities done by the user starting from latest to the first.

If users commit and pushed a file accidentally, there is still a way to go back to the state where everything works. Reflog keeps track of what have been changed for the user including switching branch.
Git commit for reflog:
```
git reflog
```
[example](http://gitready.com/images/reflog.png) reflog

and to revert changes until to the point where everything works;
```
git reset HEAD@{number of the head}
```

### HEAD

HEADs are references to latest commits on current branch and they are usually points tip of the branch. However there are HEADs refers a version which has no attachment with a branch, in these cases they are called detached HEADs

### *References*

- https://github.com/k88hudson/git-flight-rules/blob/master/README.md#i-want-to-set-a-global-user
- https://stackoverflow.com/questions/7239333/how-do-i-commit-only-some-files
- https://stackoverflow.com/questions/2745076/what-are-the-differences-between-git-commit-and-git-push
- https://stackoverflow.com/questions/7239333/how-do-i-commit-only-some-files/30043911
- https://stackoverflow.com/questions/348170/how-to-undo-git-add-before-commit
- https://stackoverflow.com/questions/1519006/how-do-you-create-a-remote-git-branch
- Kong Lam
